package pl.diminutive.find.control;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.stream.Collectors.toSet;

public class TextHandler {

    private final Set<String> diminutives = new LinkedHashSet<>();
    private final Set<String> allWords = new LinkedHashSet<>();
    private final Set<String> wordWithoutDiminutives = new LinkedHashSet<>();
    private final ExecutorService executorService = Executors.newWorkStealingPool(15);

    public Set<String> getDiminutives() {
        return diminutives;
    }

    public Set<String> getWordWithoutDiminutives() {
        return wordWithoutDiminutives;
    }

    public void processText(String text) {
        IsDiminutiveChecker checker = new IsDiminutiveChecker();
        TextScanner textScanner = new TextScanner();
        allWords.addAll(textScanner.getChunkedText(text));
        wordWithoutDiminutives.addAll(allWords);
        try {
            wordWithoutDiminutives.removeAll(executorService.submit(() ->
                    allWords.stream()
                            .parallel()
                            .filter(word -> checker.isDiminutive(word, diminutives))
                            .collect(toSet()))
                    .get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void clear() {
        diminutives.clear();
        allWords.clear();
        wordWithoutDiminutives.clear();
    }
}
