<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<!doctype html>
<html lang="pl">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="main.css"/>
    <link rel="stylesheet" href="styles.css"/>

    <title>Wyszukiwarka zdrobnień</title>
</head>
<body class="scrolled">

<header class="header-bar">
    <div class="header-bar_left">
        <div class="img-bg">
            <img src="icon.png"/>
        </div>
        <span class="subtitle">Wyszukiwarka zdrobnień</span>
    </div>
</header>


<div class="content">
    <form class="form" ACTION="../find-diminutives" id="findDimutivesInText" METHOD="POST"
          accept-charset="UTF-8">
        <label for="textForSearch">Podaj tekst:</label>
        <div class="form-input"><textarea name="textForSearch" rows="20" id="textForSearch"></textarea></div>
        <button type="submit" class="button bordered info">Wyślij</button>
    </form>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js"
        integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U"
        crossorigin="anonymous"></script>
</body>
</html>