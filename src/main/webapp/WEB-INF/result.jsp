<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Wyszukiwarka zdrobnień</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="../main.css" />
    <link rel="stylesheet" href="styles.css" />
    
</head>
<body class="scrolled">
    <header class="header-bar">
        <div class="header-bar_left">
            <div class="img-bg">
                <img src="../icon.png"/>
            </div>
            <span class="subtitle">Wyszukiwarka zdrobnień</span>
        </div>
    </header>
    <div class="result">

        <div id="chartContainer" class="chart" style="height: 200px; width: 100%;margin-top: 20px;margin-bottom: 20px"></div>
        <div class="result-content">
            <div class="result-column">
                <p>Zdrobnienia</p>
                <div>
                    <c:forEach items="${diminutives}" var="item" varStatus="loop">
                        ${item}${!loop.last ? ', ' : ''}
                    </c:forEach>
                </div>
            </div>
            <div class="result-column">
                <p>Wyrazy niezdrobniałe</p>
                <div>
                    <c:forEach items="${nonDiminutives}" var="item" varStatus="loop">
                        ${item}${!loop.last ? ', ' : ''}
                    </c:forEach>
                </div>
            </div>
        </div>

</div>




<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js"
        integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
    window.onload = function () {

        CanvasJS.addColorSet("greenShades",
            [//colorSet Array
                "#006171",
                "#39C8DE"
            ]);


        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            colorSet: "greenShades",
            data: [{
                type: "doughnut",
                startAngle: 60,
                indexLabelFontSize: 17,
                indexLabel: "{label} - #percent%",
                toolTipContent: "<b>{label}:</b> {y} (#percent%)",
                dataPoints: [
                    { y: ${diminutivesCount}, label: "Zdrobnienia" },
                    { y: ${nonDiminutivesCount}, label: "Wyrazy niezdrobniałe"},
                ]
            }]
        });
        chart.render();

    }
</script>

</body>
</html>
